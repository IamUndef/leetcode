package org.example.leetcode;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Solution30 {

    public List<Integer> findSubstring(String s, String[] words) {
        int wordCount = words.length, wordLength = words[0].length(), lineLength = wordCount * wordLength;
        if (s.length() < lineLength) {
            return Collections.emptyList();
        }
        Map<String, Integer> originWords = new HashMap<>();
        for (String word : words) {
            originWords.merge(word, 1, Integer::sum);
        }
        Map<Integer, String> indexes = new TreeMap<>();
        for (String word : originWords.keySet()) {
            int lastIndex = 0;
            while (lastIndex < s.length()) {
                int index = s.indexOf(word, lastIndex);
                if (index == -1) {
                    break;
                }
                indexes.put(index, word);
                lastIndex = index + 1;
            }
        }
        List<Integer> results = new ArrayList<>();
        List<Map.Entry<Integer, String>> orderedIndexes = new ArrayList<>(indexes.entrySet());
        for (int i = 0; i < orderedIndexes.size() - (wordCount - 1); i++) {
            int from = orderedIndexes.get(i).getKey(), to, lastTo = 0, offset = 0;
            while (i + offset < orderedIndexes.size() - (wordCount - 1)) {
                lastTo = orderedIndexes.get(i + offset + wordCount - 1).getKey();
                if (lastTo - from + wordLength == lineLength) {
                    break;
                }
                offset++;
            }
            to = lastTo;
            if (to - from + wordLength == lineLength) {
                Map<String, Integer> rangeWords = new HashMap<>();
                for (Map.Entry<Integer, String> entry : orderedIndexes.subList(i, i + offset + wordCount)) {
                    if ((entry.getKey() - from) % wordLength == 0) {
                        rangeWords.merge(entry.getValue(), 1, Integer::sum);
                    }
                }
                if (originWords.equals(rangeWords)) {
                    results.add(from);
                }
            }
        }
        return results;
    }
}
